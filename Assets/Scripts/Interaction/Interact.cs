﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public abstract class Interact : MonoBehaviour
    {
        
        public bool HasInteraction { get => isHasInteraction; }
        public bool isHasInteraction;

        public event Action OnStartInteract;
        public event Action OnEndInteract;

        protected void StartInteract()
        {
            isHasInteraction = true;
            OnStartInteract?.Invoke();
        }

        protected void StopInteract()
        {
            isHasInteraction = false;
            OnEndInteract?.Invoke();
        }


    }
}