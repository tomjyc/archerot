﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Archero
{
    public class InteractDistance : Interact
    {
        public float Distance { get => interactionDistance;}
        [SerializeField] float interactionDistance = 1.5f;

        private Transform target;

        void Start()
        {
            target = GetComponent<Character>().Target;
        }

        internal void Initialize(float interactionDistance)
        {
            this.interactionDistance = interactionDistance;
        }

        void Update()
        {
            if (target != null)
            {
                float distance = Vector3.Distance(target.position, transform.position);

                if (distance <= interactionDistance && !isHasInteraction)
                    StartInteract();
                else if (distance > interactionDistance && isHasInteraction)
                    StopInteract();
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, interactionDistance);
        }
    }
}