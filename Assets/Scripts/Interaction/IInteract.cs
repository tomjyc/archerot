﻿using System;
using UnityEngine;

namespace Task.Archero
{
    public interface IInteract
    {
        event Action OnStartInteract;
        event Action OnEndInteract;

        bool HasInteraction { get; }

    }
}