﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Archero
{
    public class InteractTrigger : Interact
    {
        private void OnTriggerEnter(Collider other)
        {
            StartInteract();
        }

        private void OnTriggerExit(Collider other)
        {
            StopInteract();
        }
    }
}