﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class Bullet : MonoBehaviour
    {
        public float speed = 5;

        int damage;
        int friendlyLayer;

        private void Start()
        {
            StartCoroutine(destroyOnTime());
        }

        public void Initialize(int damage, int friendlyLayer)
        {
            this.damage = damage;
            this.friendlyLayer = friendlyLayer;
        }

        void Update()
        {
            transform.Translate(0, 0, speed * Time.deltaTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer != friendlyLayer)
            {
                DoDamage(other);
                DestroyBullet();
            }

        }

        void DoDamage(Collider other)
        {
            if (other.GetComponent<Character>())
            {
                Character enemy = other.GetComponent<Character>();
                enemy.TakeDamage(damage);
            }
        }
        void DestroyBullet()
        {
            Destroy(gameObject);
        }
        IEnumerator destroyOnTime() 
        {
            yield return new WaitForSeconds(10);
            DestroyBullet();
        }

        public class Factory : PlaceholderFactory<Bullet> { }
    }
}