﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Archero
{
    public class IntervalBulletAttack : BulletAttack
    {
        float shootingDelay = .2f;

        public IntervalBulletAttack(float shootingDelay, Bullet.Factory bulletFactory, Enemy enemy) : base(bulletFactory, enemy)
        {
            this.shootingDelay = shootingDelay;
        }

        public override void Attack(int damage)
        {
            float delay = 0;
            for (int i = 0; i < 3; i++)
            {
                delay = shootingDelay * i;
                character.StartCoroutine(AttackDelay(damage, delay));
            }
        }

        IEnumerator AttackDelay(int damage, float delay)
        {
            yield return new WaitForSeconds(delay);

            GenerateBullet(damage, 0);
        }
    }
}