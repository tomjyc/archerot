﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Archero
{
    public interface IAttack
    {
        void Attack(int damage);
    }
}