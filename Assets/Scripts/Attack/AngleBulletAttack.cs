﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Archero
{
    public class AngleBulletAttack : BulletAttack
    {
        float bulletAngle;

        public AngleBulletAttack(float bulletAngle, Bullet.Factory bulletFactory, Enemy enemy) : base(bulletFactory, enemy)
        {
            this.bulletAngle = bulletAngle;
        }

        public override void Attack(int damage)
        {
            GenerateBullet(damage, 0);
            GenerateBullet(damage, bulletAngle);
            GenerateBullet(damage, -bulletAngle);
        }
    }
}