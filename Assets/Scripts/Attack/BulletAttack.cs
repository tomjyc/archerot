﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class BulletAttack : IAttack
    {
        protected Bullet.Factory bulletFactory;

        protected Character character;

        public BulletAttack(Bullet.Factory bulletFactory, Character character)
        {
            this.character = character;
            this.bulletFactory = bulletFactory;
        }

        public virtual void Attack(int damage)
        {
            GenerateBullet(damage, 0);
        }

        protected void GenerateBullet(int damage, float deviation)
        {
            GameObject bullet = bulletFactory.Create().gameObject;
            bullet.transform.position = character.bulletSpawnPlace.position;
            bullet.transform.rotation = SetDeviation(deviation);
            bullet.GetComponent<Bullet>().Initialize(damage, character.gameObject.layer);
        }

        private Quaternion SetDeviation(float deviation)
        {
            Vector3 lookAtTarget = character.Target.transform.position - character.transform.position;
            Quaternion rotation = Quaternion.LookRotation(lookAtTarget);

            rotation *= Quaternion.Euler(0, deviation, 0);

            return rotation;
        }
    }
}