﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class InteractAttack : IAttack
    {
        Character character;

        public InteractAttack(Character character)
        {
            this.character = character;
        }

        public void Attack(int damage)
        {
            character.Target.GetComponent<Character>().TakeDamage(damage);
        }
    }
}