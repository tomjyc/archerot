﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class ProjectInstaller : MonoInstaller<ProjectInstaller>
    {
        public GameObject[] levelList;

        public override void InstallBindings()
        {
            Container.BindInstances(levelList);
            Container.BindInterfacesAndSelfTo<GameManager>().AsSingle();
        }
    }
}