using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class SceneInstaller : MonoInstaller<SceneInstaller>
    {
        public Transform ui_Health;

        [Inject]
        Settings settings = null;

        public override void InstallBindings()
        {
            Container.BindFactory<GameObject, LevelManager, LevelManager.Factory>().FromFactory<CustomLevelFactory>();
            Container.BindFactory<Player, Player.Factory>().FromComponentInNewPrefab(settings.pref_player);

            Container.BindInterfacesAndSelfTo<CharacterManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<LevelController>().AsSingle();

            Container.BindFactory<GameObject, Enemy, Enemy.Factory>().FromFactory<CustomEnemyFactory>();

            Container.BindFactory<Bullet, Bullet.Factory>().FromComponentInNewPrefab(settings.BulletPrefab);
            Container.BindFactory<HealthUI, HealthUI.Factory>().FromComponentInNewPrefab(settings.HealthBarPrefab).UnderTransform(ui_Health);

            SignalBusInstaller.Install(Container);
            Container.DeclareSignal<SignalEnemyDie>();
            Container.DeclareSignal<SignalPlayerDie>();
            Container.DeclareSignal<SignalPlayerWin>();
        }

        [Serializable]
        public class Settings
        {
            public GameObject BulletPrefab;
            public GameObject pref_player;
            public GameObject HealthBarPrefab;
        }
    }
}