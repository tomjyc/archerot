﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    [CreateAssetMenu(menuName = "Asteroids/Game Settings")]
    public class ArcheroSettingsInstaller : ScriptableObjectInstaller<ArcheroSettingsInstaller>
    {
        public SceneInstaller.Settings GameInstaller;

        public override void InstallBindings()
        {
            Container.BindInstance(GameInstaller);
        }
    }
}