﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Task.Archero
{
    public class RunState : State
    {
        IMotor motor;
        NavMeshAgent agent;

        float realSpeed;

        public RunState(IMotor motor, NavMeshAgent agent)
        {
            this.motor = motor;
            this.agent = agent;
        }

        public override void EnterState()
        {
            realSpeed = agent.speed;
            agent.speed = realSpeed * 2;
        }

        public override void ExitState()
        {
            motor.StopMove();
            agent.speed = realSpeed;
        }

        public override void UpdateState()
        {
            motor.Move();
        }
    }
}