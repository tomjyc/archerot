﻿
namespace Task.Archero
{
    public class StopState : State
    {
        IMotor motor;

        public StopState(Enemy enemy, StatsData data)
        {
            motor = new FollowMotor(enemy, data);
        }

        public override void EnterState()
        {
            motor.StopMove();
        }

        public override void ExitState()
        {

        }

        public override void UpdateState()
        {

        }
    }
}