﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Archero
{
    public class CastState : State
    {
        private Enemy enemy;
        Transform target;

        LookAtTarget lookAt;

        public CastState(Enemy enemy)
        {
            this.enemy = enemy;
            lookAt = new LookAtTarget(enemy);
        }

        public override void EnterState()
        {
            target = enemy.Target;
        }

        public override void ExitState()
        {
            target = null;
        }

        public override void UpdateState()
        {
            lookAt.Look(target);
        }
    }
}