﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Archero
{
    public class FollowState : State
    {
        private IMotor motor;

        public FollowState(IMotor motor)
        {
            this.motor = motor;
        }

        public override void EnterState()
        {

        }

        public override void ExitState()
        {
            motor.StopMove();
        }

        public override void UpdateState()
        {
            motor.Move();
        }
    }
}