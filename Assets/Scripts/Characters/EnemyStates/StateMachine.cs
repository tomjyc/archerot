﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Task.Archero
{
    public struct NextState
    {
        public State state;
        public float timeToNextState;

        public NextState(State nextstate, float timeToNextState)
        {
            this.state = nextstate;
            this.timeToNextState = timeToNextState;
        }
    }

    public class StateMachine
    {
        public event Action EndState;

        public State currentState { get; private set; }

        IEnumerator activeCoroutine;

        Enemy enemy;

        public StateMachine(Enemy enemy)
        {
            currentState = null;
            this.enemy = enemy;
        }

        public void ChangeState(NextState nextState)
        {
            ChangeState(nextState.state);
            activeCoroutine = EndStateTimer(nextState.timeToNextState);
            enemy.StartCoroutine(activeCoroutine);
        }
        public void ChangeState(State state)
        {
            if (currentState != null) currentState.ExitState();
            if (activeCoroutine != null) enemy.StopCoroutine(activeCoroutine);

            currentState = state;
            currentState.EnterState();
        }

        IEnumerator EndStateTimer(float delay)
        {
            yield return new WaitForSeconds(delay);
            EndState?.Invoke();
        }

        public void Update()
        {
            if(currentState != null)
                currentState.UpdateState();
        }
    }

    public abstract class State
    {
        public abstract void EnterState();
        public abstract void ExitState();
        public abstract void UpdateState();
    }
}