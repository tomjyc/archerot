﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Archero
{
    public class InteractAttackState : State
    {
        int attackDamage;

        Enemy enemy;

        IAttack attack;
        Interact interact;

        IEnumerator attackCoroutine;

        public InteractAttackState(Enemy enemy, int attackDamage)
        {
            this.attackDamage = attackDamage;
            this.enemy = enemy;

            attack = new InteractAttack(enemy);
            interact = enemy.GetComponent<InteractDistance>();

            interact.OnStartInteract += OnStartInteract;
            interact.OnEndInteract += OnEndInteract;
        }

        void OnStartInteract()
        {
            StopAttack();

            StartAttack();
        }
        void OnEndInteract()
        {
            StopAttack();
        }

        void StartAttack()
        {
            attackCoroutine = InteractAttack();
            enemy.StartCoroutine(attackCoroutine);
        }
        public void StopAttack()
        {
            if (attackCoroutine != null)
                enemy.StopCoroutine(attackCoroutine);
        }
        IEnumerator InteractAttack()
        {

            attack.Attack(attackDamage);

            yield return new WaitForSeconds(1);

            if (interact.HasInteraction)
                StartAttack();
        }

        public override void EnterState()
        {

        }
        public override void ExitState()
        {

        }
        public override void UpdateState()
        {

        }
    }
}