﻿using System;
using UnityEngine;

namespace Task.Archero
{
    public class AttackState : State
    {
        IAttack attack;
        int attackDamage;

        public AttackState(IAttack attack, int attackDamage)
        {
            this.attack = attack;
            this.attackDamage = attackDamage;
        }

        public override void EnterState()
        {
            attack.Attack(attackDamage);
        }

        public override void ExitState()
        {

        }

        public override void UpdateState()
        {
            
        }
    }
}