﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Task.Archero
{
    public class LookAtTarget
    {
        NavMeshAgent agent;
        Transform charPosition;

        public LookAtTarget(Character character)
        {
            agent = character.GetComponent<NavMeshAgent>();
            charPosition = character.transform;
        }

        public void Look(Transform targetPos)
        {
            agent.updateRotation = false;
            Vector3 direction = targetPos.position - charPosition.position;
            Quaternion rotate = Quaternion.LookRotation(direction);
            charPosition.rotation = Quaternion.Slerp(charPosition.rotation, rotate, .3f);
            agent.updateRotation = true;
        }
    }
}