﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class EnemyBoss : Enemy
    {
        List<NextState> randStates;
        NextState state_prev, state_rest, state_cast;

        IMotor runToEmnemy, randomMotor;
        IAttack threeBullet, angleThreeBullet;

        [Inject] Bullet.Factory bulletfactory;

        protected override void Awake()
        {
            base.Awake();
            randomMotor = new RandomMotor(this, stats.Data);
            runToEmnemy = new FollowMotor(this, stats.Data);

            threeBullet = new IntervalBulletAttack(.2f, bulletfactory, this);
            angleThreeBullet = new AngleBulletAttack(30, bulletfactory, this);
        }

        protected override void Start()
        {
            base.Start();

            randStates = new List<NextState>();

            
            state_rest = new NextState(new FollowState(randomMotor), 2f);

            state_cast = new NextState(new CastState(this), 1f);

            State state = new RunState(runToEmnemy, agent);
            randStates.Add(new NextState(state, 2f));

            state = new AttackState(threeBullet, stats.getAttackDamage());
            randStates.Add(new NextState(state, 1f));

            state = new AttackState(angleThreeBullet, stats.getAttackDamage());
            randStates.Add(new NextState(state, 1f));

            stateMachine = new StateMachine(this);
            stateMachine.EndState += EndState;

            EndState();
        }

        void EndState()
        {
            if (states.Count == 0)
                GenerateStates();

            NextState state = states.Dequeue();
            stateMachine.ChangeState(state);

        }

        void GenerateStates()
        {
            states.Enqueue(state_rest);
            states.Enqueue(state_cast);

            NextState state;
            do
            {
                int randNum = Random.Range(0, randStates.Count);
                state = randStates[randNum];
            } 
            while (state.Equals(state_prev));

            states.Enqueue(state);
            state_prev = state;
        }
    }
}