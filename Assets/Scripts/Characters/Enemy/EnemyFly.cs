﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class EnemyFly : Enemy
    {
        IMotor motor;
        IAttack attack;

        protected override void Awake()
        {
            base.Awake();
            motor = new RandomMotor(this, stats.Data);
        }

        protected override void Start()
        {
            base.Start();

            State state = new FollowState(motor);
            states.Enqueue(new NextState(state, 5f));

            state = new CastState(this);
            states.Enqueue(new NextState(state, 1f));

            attack = new BulletAttack(bulletfactory, this);
            state = new AttackState(attack, stats.getAttackDamage());
            states.Enqueue(new NextState(state, 1f));

            stateMachine.EndState += EndState;

            EndState();
        }

        protected override void Update()
        {
            base.Update();
        }

        void EndState()
        {
            NextState state = states.Dequeue();
            stateMachine.ChangeState(state);
            states.Enqueue(state);
        }

        private void OnDrawGizmos()
        {
            ((RandomMotor)motor).Gizmo();
        }
    }
}