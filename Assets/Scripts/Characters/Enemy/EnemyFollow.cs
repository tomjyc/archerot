﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class EnemyFollow : Enemy
    {
        FollowState followState;

        IMotor motor;

        protected override void Awake()
        {
            base.Awake();
            motor = new FollowMotor(this, stats.Data);
        }

        protected override void Start()
        {
            base.Start();

            followState = new FollowState(motor);
            stateMachine.ChangeState(followState);
        }

        protected override void Update()
        {
            base.Update();
        }
    }
}