﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public abstract class Enemy : Character
    {

        protected Queue<NextState> states;
        protected StateMachine stateMachine;

        public State stopState { get; private set; }
        InteractAttackState interactAttack;

        protected override void Awake()
        {
            base.Awake();

            stats = new CharacterStats(this, healthUI);
            stateMachine = new StateMachine(this);
        }

        protected virtual void Start()
        {

            states = new Queue<NextState>();
            stopState = new StopState(this, stats.Data);

            interactAttack = new InteractAttackState(this, stats.getAttackDamage());
            
        }

        protected virtual void Update()
        {
            stateMachine.Update();
        }

        public override void Die()
        {
            signalBus.Fire(new SignalEnemyDie() { enemy = this });
            Destroy(gameObject);
        }
        internal void Stop()
        {
            interactAttack.StopAttack();
            stateMachine.ChangeState(stopState);
        }

        public class Factory : PlaceholderFactory<GameObject, Enemy> { }
        
    }
}