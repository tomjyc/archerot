﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class CustomEnemyFactory : IFactory<GameObject, Enemy>
    {
        DiContainer container;

        public CustomEnemyFactory(DiContainer container)
        {
            this.container = container;
        }

        public Enemy Create(GameObject v)
        {
            return container.InstantiatePrefabForComponent<Enemy>(v);
        }
    }
}