﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Task.Archero
{
    public class HealthUI : MonoBehaviour
    {
        Transform target;
        Image healthSlider;

        Transform cam;

        private void Start()
        {
            cam = Camera.main.transform;
        }
        public void Initialize(Transform target, CharacterStats stats)
        {
            this.target = target;

            healthSlider = transform.GetChild(0).GetComponent<Image>();

            stats.onHealthChanged += OnHealthChanged;
            OnHealthChanged(stats.Data.maxHealth, stats.Data.currentHealth);
        }

        private void LateUpdate()
        {
            transform.position = target.position;
            transform.forward = -cam.forward;
        }

        void OnHealthChanged(int maxHealth, int currentHealth)
        {
            float healthPercent = currentHealth / (float)maxHealth;
            healthSlider.fillAmount = healthPercent;
            
            if (currentHealth <= 0)
                Destroy(gameObject);
        }

        public class Factory : PlaceholderFactory<HealthUI> { }
    }
}