﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class CustomPlayerFactory 
    {
        DiContainer container;

        public CustomPlayerFactory(DiContainer container)
        {
            this.container = container;
        }

        public Player Create(GameObject v)
        {
            return container.InstantiatePrefabForComponent<Player>(v);
        }
    }
}