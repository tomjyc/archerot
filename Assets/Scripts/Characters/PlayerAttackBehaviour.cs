﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class PlayerAttackBehaviour : StateMachineBehaviour
    {
        [Inject] protected LevelController levelController;

        Player player;
        LookAtTarget lookAt;

        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            player = animator.GetComponentInParent<Player>();
            lookAt = new LookAtTarget(player);
            SetNearestTarget();
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (player.Target != null && player.isCaneShoot)
            {
                lookAt.Look(player.Target);
                animator.SetTrigger("Attack");
            }
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.ResetTrigger("Attack");
        }

        void SetNearestTarget()
        {
            if (levelController.GetEnemies().Count > 0)
            {
                float minDistance = float.MaxValue;
                foreach (Enemy enemy in levelController.GetEnemies())
                {
                    float distanceToenemy = Vector3.Distance(enemy.transform.position, player.transform.position);
                    if (distanceToenemy < minDistance)
                    {
                        minDistance = distanceToenemy;
                        player.Target = enemy.transform;
                    }
                }
            }
        }

    }
}