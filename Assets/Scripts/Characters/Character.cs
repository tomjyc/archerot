﻿using UnityEngine;
using UnityEngine.AI;
using Zenject;

namespace Task.Archero
{
    public abstract class Character : MonoBehaviour
    {
        [Inject] protected HealthUI.Factory healthUI;
        [Inject] protected SignalBus signalBus;
        [Inject] protected Bullet.Factory bulletfactory;

        public Transform Target { get => target; set => target = value; }
        protected Transform target;

        public Transform bulletSpawnPlace{ get; protected set; }
        public Transform healthUITarget { get; protected set; }

        public NavMeshAgent Agent { get => agent; }
        protected NavMeshAgent agent;
        
        protected CharacterStats stats;

        protected virtual void Awake()
        {
            agent = GetComponent<NavMeshAgent>();

            bulletSpawnPlace = transform.Find("Bullet Spawn Place");
            healthUITarget = transform.Find("Health UI Target");
        }

        public virtual void Attack() { }
        public virtual void Die() { }
        public void TakeDamage(int damage)
        {
            stats.TakeDamage(damage);
        }
    }
}