﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Archero
{
    public class StatsData
    {
        private CharSettings charSettings;

        public float speed;
        public int maxHealth;
        public int currentHealth;
        public int damage;
        public float attackDelay;

        public StatsData(CharSettings charSettings)
        {
            speed = charSettings.speed;
            maxHealth = charSettings.maxHealth;
            currentHealth = maxHealth;
            damage = charSettings.damage;
            attackDelay = charSettings.attackDelay;
        }
    }

    public class CharacterStats
    {
        public event System.Action<int, int> onHealthChanged;

        Character character;
        HealthUI healthUI;

        public StatsData Data { get => data; set => data = value; }
        StatsData data;

        public CharacterStats(Character character, HealthUI.Factory healthUIFactory)
        {
            LoadCharData charData = new LoadCharData(character.name);
            data = new StatsData(charData.Load());

            Constructor(character, healthUIFactory);
        }

        public CharacterStats(Character character, HealthUI.Factory healthUIFactory, StatsData playerData) : this(character, healthUIFactory)
        {
            data = playerData;
            Constructor(character, healthUIFactory);
        }

        void Constructor(Character character, HealthUI.Factory healthUIFactory)
        {
            this.character = character;
            healthUI = healthUIFactory.Create().GetComponent<HealthUI>();
            healthUI.Initialize(character.healthUITarget, this);
            healthUI.gameObject.SetActive(true);
        }

        public void TakeDamage(int damage)
        {
            data.currentHealth -= damage;

            if(onHealthChanged != null)
                onHealthChanged(data.maxHealth, data.currentHealth);

            if (data.currentHealth <= 0)
                Die();
        }

        protected void Die()
        {
            character.Die();
        }

        public int getAttackDamage()
        {
            return data.damage;
        }
    }
}
