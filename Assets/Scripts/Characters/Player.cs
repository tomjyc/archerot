﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class Player : Character
    {
        [Inject] FloatingJoystick joystick;
        [Inject] GameManager gameManager;

        IMotor motor;
        IAttack attack;

        float attackDelay = 3f; // tmp
        public bool isCaneShoot = true;

        protected override void Awake()
        {
            base.Awake();

            if (gameManager.PlayerData != null)
                stats = new CharacterStats(this, healthUI, gameManager.PlayerData);
            else
            {
                LoadCharData loadData = new LoadCharData(name);
                stats = new CharacterStats(this, healthUI);
            }


            attackDelay = stats.Data.attackDelay;

            motor = new PlayerMotor(this, joystick, stats.Data);
            attack = new BulletAttack(bulletfactory, this);
        }

        private void Update()
        {
            motor.Move();
        }

        public override void Attack()
        {
            attack.Attack(stats.getAttackDamage());
            StartCoroutine(resetAttackTimer());
        }
        
        IEnumerator resetAttackTimer()
        {
            isCaneShoot = false ;
            yield return new WaitForSeconds(attackDelay);
            isCaneShoot = true;
        }

        public override void Die()
        {
            signalBus.Fire<SignalPlayerDie>();
            Destroy(gameObject);
        }
        public StatsData getPlayerData()
        {
            return stats.Data;
        }

        public class Factory : PlaceholderFactory<Player> { }
    }
}