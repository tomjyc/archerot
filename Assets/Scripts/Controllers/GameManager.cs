﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class GameManager : IInitializable
    {
        public Queue<GameObject> levels;
        GameObject[] levelList;

        public StatsData PlayerData { get => playerData; set => playerData = value; }
        StatsData playerData;

        public GameManager(GameObject[] levelList)
        {
            this.levelList = levelList;
            levels = new Queue<GameObject>();
        }

        public void Initialize()
        {
            Reset();
        }

        public void Reset()
        {
            levels.Clear();
            playerData = null;
            foreach (GameObject level in levelList)
                levels.Enqueue(level);
        }
    }
}