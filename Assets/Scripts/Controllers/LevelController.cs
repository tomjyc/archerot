﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class LevelController : IInitializable, IDisposable
    {
        [Inject] GameManager gameManager;

        CharacterManager manager;
        SignalBus signalBus;

        const int mask_win = (1 << 0) + (0 << 1) + (1 << 2) + (0 << 3) + (1 << 4);

        public LevelController(CharacterManager manager, SignalBus signalBus)
        {
            this.manager = manager;
            this.signalBus = signalBus;
        }

        public void Initialize()
        {
            signalBus.Subscribe<SignalEnemyDie>(EnemyDie);
            signalBus.Subscribe<SignalPlayerDie>(PlayerDie);
        }

        public void Dispose()
        {
            signalBus.Unsubscribe<SignalEnemyDie>(EnemyDie);
            signalBus.Unsubscribe<SignalPlayerDie>(PlayerDie);
        }

        public List<Enemy> GetEnemies()
        {
            return manager.GetEnemies();
        }
        public Player GetPlayer()
        {
            return manager.GetPlayer();
        }

        void EnemyDie(SignalEnemyDie signal)
        {
            manager.RemoveEnemy(signal.enemy);
            if (GetEnemies().Count <= 0)
            {
                Debug.Log("You Win !!!");
                if (gameManager.levels.Count > 0)
                    GetPlayer().Agent.areaMask = mask_win;
                else
                    signalBus.Fire<SignalPlayerWin>();
            }
        }
        void PlayerDie()
        {
            Debug.Log("You Lose !!!");
            manager.StopAllEnemies();
        }
    }
}