﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UnityEngine.SceneManagement;
using System;

namespace Task.Archero
{
    public class UIController : MonoBehaviour, IDisposable
    {
        [Inject] SignalBus signalBus;
        [Inject] GameManager gameManager;
        GameObject panel_restart;

        void Start()
        {
            panel_restart = transform.Find("Panel Restart").gameObject;
            signalBus.Subscribe<SignalPlayerDie>(PlayerDie);
            signalBus.Subscribe<SignalPlayerWin>(PlayerWin);
        }

        public void Dispose()
        {
            signalBus.Unsubscribe<SignalPlayerDie>(PlayerDie);
            signalBus.Unsubscribe<SignalPlayerWin>(PlayerWin);
        }

        void PlayerWin()
        {
            ActiveRestartPanel();
        }
        void PlayerDie()
        {
            ActiveRestartPanel();
        }

        void ActiveRestartPanel()
        {
            panel_restart.gameObject.SetActive(true);
        }

        public void RestartGame()
        {
            gameManager.Reset();
            SceneManager.LoadScene(0);
        }
        
    }
}