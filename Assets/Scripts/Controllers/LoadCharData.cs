﻿using System.IO;
using System;
using UnityEngine;

namespace Task.Archero
{
    public class CharSettings
    {
        public float speed;
        public int maxHealth;
        public int damage;
        public float attackDelay;

        public CharSettings GetStandartValues()
        {
            return new CharSettings
            {
                maxHealth = 100,
                speed = 3.5f,
                damage = 10,
                attackDelay = 2f
            };
        }
    }

    public class LoadCharData
    {
        string fileName;

        public LoadCharData(string fileName)
        {
            this.fileName = fileName;
        }

        public void Save(CharSettings settings)
        {
            string json = JsonUtility.ToJson(settings);

            File.WriteAllText(Application.dataPath + "/Resources/CharSettings/" + fileName + ".txt", json);
        }

        public CharSettings Load()
        {
            CharSettings settings;

            var textFile = Resources.Load<TextAsset>("CharSettings/" + fileName);

            if (textFile != null)
            {
                string file = textFile.ToString();
                settings = JsonUtility.FromJson<CharSettings>(file);
            }
            else
            {
                settings = new CharSettings().GetStandartValues();
                Save(settings);
            }

            return settings;
        }
    }
}