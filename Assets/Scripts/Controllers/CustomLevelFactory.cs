﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class CustomLevelFactory : IFactory<GameObject, LevelManager>
    {
        DiContainer container;

        public CustomLevelFactory(DiContainer container)
        {
            this.container = container;
        }

        public LevelManager Create(GameObject v)
        {
            return container.InstantiatePrefabForComponent<LevelManager>(v);
        }
    }
}