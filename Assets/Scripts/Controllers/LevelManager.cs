﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class LevelManager : MonoBehaviour
    {
        public SpawnPlace[] characterSpawners;

        public class Factory : PlaceholderFactory<GameObject, LevelManager> { }
    }
}