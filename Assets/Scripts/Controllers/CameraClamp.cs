﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class CameraClamp : MonoBehaviour
    {
        [Inject] CharacterManager manager;
        public Vector2 limits = new Vector2(3, 5);
        public Vector2 offset = new Vector2(3, 5);
        Transform player;
        float yPos;

        void Start()
        {
            player = manager.GetPlayer().transform;
            yPos = transform.position.y;
        }

        private void Update()
        {
            FollowTarget(player);
        }

        void LateUpdate()
        {
            transform.localPosition = new Vector3(Mathf.Clamp(transform.position.x, -limits.x + offset.x, limits.x + offset.x), yPos, Mathf.Clamp(transform.position.z, -limits.y + offset.y, limits.y + offset.y));
        }

        void FollowTarget(Transform target)
        {
            if(target != null)
            {
                transform.position = new Vector3(target.position.x, yPos, target.position.z);
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(new Vector3(-limits.x + offset.x, transform.position.y, -limits.y + offset.y), new Vector3(-limits.x + offset.x, transform.position.y, limits.y + offset.y));
            Gizmos.DrawLine(new Vector3(-limits.x + offset.x, transform.position.y, limits.y + offset.y), new Vector3(limits.x + offset.x, transform.position.y, limits.y + offset.y));
            Gizmos.DrawLine(new Vector3(limits.x + offset.x, transform.position.y, limits.y + offset.y), new Vector3(limits.x + offset.x, transform.position.y, -limits.y + offset.y));
            Gizmos.DrawLine(new Vector3(limits.x + offset.x, transform.position.y, -limits.y + offset.y), new Vector3(-limits.x + offset.x, transform.position.y, -limits.y + offset.y));
        }
    }
}