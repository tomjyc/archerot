﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Task.Archero
{
    public class ToNextSceneTrigger : MonoBehaviour
    {
        [Inject] GameManager gameManager;
        [Inject] LevelController levelController;
        InteractTrigger interact;

        void Start()
        {
            interact = GetComponent<InteractTrigger>();
            interact.OnStartInteract += ToNextScene;
        }

        void ToNextScene()
        {
            gameManager.PlayerData = levelController.GetPlayer().getPlayerData();
            SceneManager.LoadScene(0);
        }
    }
}