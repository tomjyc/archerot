﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Archero
{
    public class AnimationEventManager : MonoBehaviour
    {
        Character character;

        private void Awake()
        {
            character = GetComponentInParent<Character>();
        }

        public void AttackEvent()
        {
            character.Attack();
        }
    }
}