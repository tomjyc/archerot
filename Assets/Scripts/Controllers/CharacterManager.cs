﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Task.Archero
{
    public class CharacterManager : IInitializable
    {
        [Inject] GameManager gameManager;

        List<Enemy> enemies = new List<Enemy>();
        readonly Enemy.Factory characterFactory;
        readonly Player.Factory playerFactory;
        readonly LevelManager.Factory levelFactory;

        Player player;
        LevelManager levelManager;

        public CharacterManager(Enemy.Factory characterFactory, Player.Factory playerFactory, LevelManager.Factory levelFactory)
        {
            this.characterFactory = characterFactory;
            this.playerFactory = playerFactory;
            this.levelFactory = levelFactory;
        }

        public void Initialize()
        {
            levelManager = levelFactory.Create(gameManager.levels.Dequeue());
            player = playerFactory.Create();

            for (int i = 0; i < levelManager.characterSpawners.Length; i++)
            {
                Enemy enemy = characterFactory.Create(levelManager.characterSpawners[i].prefab);
                enemy.transform.position = levelManager.characterSpawners[i].transform.position;
                enemy.Target = player.transform;
                enemies.Add(enemy);
                enemy.gameObject.SetActive(true);
            }
        }

        internal void StopAllEnemies()
        {
            foreach(Enemy enemy in enemies)
                enemy.Stop();
        }

        public void SpawnEnemies()
        {
            //Enemy enemy = enemies.Dequeue();
            //enemy.gameObject.SetActive(true);
        }

        public List<Enemy> GetEnemies()
        {
            return enemies;
        }
        public void RemoveEnemy(Enemy enemy)
        {
            enemies.Remove(enemy);
        }
        public Player GetPlayer()
        {
            return player;
        }
    }
}