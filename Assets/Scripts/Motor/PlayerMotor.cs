﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

namespace Task.Archero
{
    public class PlayerMotor : IMotor
    {
        FloatingJoystick joystick;
        MovePoint movePoint;

        NavMeshAgent agent;
        Animator animator;

        public PlayerMotor(Player player, FloatingJoystick joystick, StatsData data)
        {
            this.joystick = joystick;
            agent = player.Agent;
            agent.speed = data.speed;

            animator = player.transform.GetChild(0).GetComponent<Animator>();
            movePoint = player.transform.Find("Move Point").GetComponent<MovePoint>();
        }

        public void Move()
        {
            MoveAgent();
            MoveAnimation();
        }

        public void StopMove()
        {
            agent.ResetPath();
        }

        void MoveAgent()
        {
            movePoint.SetCursorPosition(new Vector2(joystick.Horizontal, joystick.Vertical));
            agent.SetDestination(movePoint.GetPosition());
        }
        void MoveAnimation()
        {
            float speedPercent = agent.velocity.magnitude / agent.speed;
            animator.SetFloat("SpeedPercent", speedPercent);
        }
    }
}