﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Task.Archero
{
    public class RandomMotor : IMotor
    {
        NavMeshAgent agent;
        Enemy enemy;

        Vector3 targetPos;
        float maxDistance = 15f;

        public RandomMotor(Enemy enemy, StatsData data)
        {
            this.enemy = enemy;
            agent = enemy.Agent;
            agent.speed = data.speed;
            agent.stoppingDistance = enemy.GetComponent<InteractDistance>().Distance;
        }

        public void Move()
        {
            if (!agent.hasPath) targetPos = GenerateDirection();

            agent.SetDestination(targetPos);
        }

        public void StopMove()
        {
            agent.ResetPath();
        }

        Vector3 GenerateDirection()
        {
            Vector3 finalPos;
            do { 
                Vector3 randomDirection = Random.insideUnitSphere * maxDistance;
                randomDirection += enemy.transform.position;

                NavMeshHit hit;
                NavMesh.SamplePosition(randomDirection, out hit, maxDistance, 1);

                finalPos = hit.position;
            }while (Vector3.Distance(finalPos, enemy.transform.position) <= 5f);

            return finalPos;
        }
        
        public void Gizmo()
        {
            Gizmos.DrawWireSphere(enemy.transform.position, maxDistance);
            Gizmos.color = Color.magenta;
            Gizmos.DrawSphere(targetPos, .2f);
        }
    }
}