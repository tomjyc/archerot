﻿using UnityEngine;

namespace Task.Archero
{
    public interface IMotor
    {
        void Move();
        void StopMove();
    }
}