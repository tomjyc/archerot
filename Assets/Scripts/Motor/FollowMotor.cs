﻿using UnityEngine;
using UnityEngine.AI;

namespace Task.Archero
{
    public class FollowMotor : IMotor
    {
        Transform target;
        NavMeshAgent agent;

        public FollowMotor(Enemy enemy, StatsData data)
        {
            target = enemy.Target;
            agent = enemy.Agent;
            agent.speed = data.speed;
            agent.stoppingDistance = enemy.GetComponent<InteractDistance>().Distance;
        }

        public void Move()
        {
            agent.SetDestination(target.position);
        }

        public void StopMove()
        {
            agent.ResetPath();
        }

    }
}