﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Task.Archero
{
    public class MovePoint : MonoBehaviour
    {

        Vector2 cursorPosition;

        void Update()
        {
            Vector3 direction = Vector3.forward * cursorPosition.y + Vector3.right * cursorPosition.x;
            direction.y = 1;
            transform.position = transform.parent.position + direction;
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, .1f);
        }

        public void SetCursorPosition(Vector2 pos)
        {
            cursorPosition = pos;
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }
    }
}